<div class="col-md-12">

  <div class="row form-group">
    <div class="col-md-4">
     <h4><strong>Ocupaciones Anteriores</strong></h4>
    </div>
    <div class="col-md-2">

    </div>
    <div class="col-md-6 text-right">
      <button type="button" name="button" class="btn btn-sm btn-success">Guardar</button>
    </div>
  </div>


    <div class="box box-default">
        <div class="box-header with-border">Información de Paciente
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row col-md-12">


                <div class="row form-group ">
                  <div class="col-md-3">
                    <div class="input-group">
                      <span class="span-width input-group-addon" style="padding-top:8px;padding-bottom:9px;">Fecha</span>
                       <input type="date" class="form-control" aria-label="...">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group">
                      <span class="span-width input-group-addon" style="padding-top:8px;padding-bottom:9px;">Empresa-lugar</span>
                       <input type="text" class="form-control" aria-label="...">
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="input-group">
                      <span class="span-width input-group-addon" style="padding-top:8px;padding-bottom:9px;">Rubro</span>
                       <input type="text" class="form-control" aria-label="...">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="input-group">
                      <span class="span-width input-group-addon" style="padding-top:8px;padding-bottom:9px;">Altitud</span>
                       <input type="text" class="form-control" aria-label="...">
                    </div>
                  </div>
                </div>

                <div class="row form-group ">

                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="span-width input-group-addon" style="padding-top:8px;padding-bottom:9px;">Área de la Empresa</span>
                       <input type="text" class="form-control" aria-label="...">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="span-width input-group-addon" style="padding-top:8px;padding-bottom:9px;">Ocupación &nbsp &nbsp &nbsp &nbsp</span>
                       <input type="text" class="form-control" aria-label="...">
                    </div>
                  </div>

                </div>

                <div class="row form-group ">

                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="span-width input-group-addon" style="padding-top:8px;padding-bottom:9px;">Tiempo de Labor en Superficie</span>
                      <select class="form-control" name="">
                          <option value=""></option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="span-width input-group-addon" style="padding-top:8px;padding-bottom:9px;">Tiempo de Labor en Socavón</span>
                      <select class="form-control" name="">
                          <option value=""></option>
                      </select>
                    </div>
                  </div>

                </div>

                <div class="row form-group ">

                  <div class="col-md-4">
                      <div class="row form-group">
                        <div class="col-md-12">
                          <h5>Riesgos Ocupacionales</h5>
                              <div class="" style="overflow:scroll;">
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox"> Ninguno
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox"> Ruido
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox"> Vibracion
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox"> Iluminacion
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox"> Temperatura
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox"> Ruido
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox"> Vibracion
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox"> Iluminacion
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox"> Temperatura
                                    </label>
                                  </div>
                              </div>
                          </div>
                      </div>

                  </div>
                  <div class="col-md-4">
                    <div class="row form-group">
                        <h5>Medios de Protección</h5>
                        <input type="text" class="form-control" name="" value="">
                    </div>
                    <div class="row form-group">
                      <h5>Accidentes Ocupacionales</h5>
                        <input type="text" class="form-control" name="" value="">
                    </div>
                    <div class="row form-group">
                      <h5>Enfermedad Ocupacional</h5>
                        <input type="text" class="form-control" name="" value="">
                    </div>
                  </div>

                  <div class="col-md-4 form-group">
                    <div class="input-group">
                      <span class="input-group-addon" id="basic-addon2">  <input type="checkbox" aria-label=""></span>
                    <input type="text" class="form-control" style="background:#FFF;" value="Uso de EPP" readonly aria-describedby="basic-addon2">
                    </div>


              <br>
                    <textarea name="name" style="resize:none;" rows="8" cols="50"></textarea>


                  </div>

                </div>


            </div>
        </div>
    </div>

  <div class="row form-group">
{{-- <hr>
    <table class="table">

      <thead>
          <tr>
            <th>Fecha Inicio</th>
            <th>Fecha Fin</th>
            <th>Empresa</th>
            <th>Actividad</th>
            <th>Area Empresa</th>
            <th>Ocupacion</th>
            <th>Tiempo - superficie</th>
            <th>Tiempo - Socavón</th>
            <th>Riesgos ocupacionales</th>
            <th>Medios Protección</th>
            <th>Accidentes Ocupacionales</th>
            <th>Enfermedad Ocupacional</th>
            <th>Uso de EPP</th>
            <th>Tipos de Produccion</th>
          </tr>
      </thead>

      <tbody>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
<td></td>
            <td></td>
            <td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
            <td></td>
          </tr>
      </tbody>

    </table> --}}

  </div>


</div>
