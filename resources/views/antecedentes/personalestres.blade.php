

{{-- <h3>PERSONALES UNO</h3> --}}
<div class="col-md-12">
  <div class="row form-group">
    <div class="col-md-2">
    </div>
    <div class="col-md-4">
    </div>
    <div class="col-md-6 text-right">
      <button type="button" name="button" class="btn btn-sm btn-success">Guardar</button>
    </div>
  </div>


    <div class="box box-default">
        <div class="box-header with-border">  <h5> <strong> Hospitalización </strong> </h5>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row col-md-12">

              <div class="row form-group">

                <div class="col-md-8">
                  <div class="input-group">
                    <span class="span-width input-group-addon" style="padding-top:8px;padding-bottom:9px;"> Motivo &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp</span>
                    <input type="text" class="form-control" aria-label="...">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="input-group ">
                    <span class="input-group-addon span-width">Año Dx.</span>
                    <input type="text" class="form-control" aria-label="...">
                  </div>
                </div>
                <div class="col-md-2">
                  <button type="button" class="btn btn-success"><span><i class="fa fa-plus" aria-hidden="true"></i></span></button>
                </div>
              </div>

              <div class="row form-group">
                <table class="table">
                   <thead>
                     <tr>
                       <th>Motivo</th>
                       <th>Año</th>
                     </tr>
                   </thead>
                   <tbody>
                     <tr>
                       <td>Hernia </td><td>1998</td>
                     </tr>
                   </tbody>
                </table>
              </div>

            </div>
        </div>
    </div>



      <div class="box box-default">
          <div class="box-header with-border">    <h5> <strong> Accidentes Laborales </strong>  </h5>
              <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                      <i class="fa fa-plus"></i>
                  </button>
              </div>
          </div>
          <div class="box-body">
              <div class="row col-md-12">

                <div class="row form-group">


                  <div class="col-md-8">
                    <div class="input-group">
                      <span class="span-width input-group-addon" style="padding-top:8px;padding-bottom:9px;">Descripción &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp</span>
                      <input type="text" class="form-control" aria-label="...">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="input-group ">
                      <span class="input-group-addon span-width">Año Dx.</span>
                      <input type="text" class="form-control" aria-label="...">
                    </div>
                  </div>
                  <div class="col-md-1">
                    <button type="button" class="btn btn-success"><span><i class="fa fa-plus" aria-hidden="true"></i></span></button>
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-md-8">
                    <div class="input-group ">
                      <span class="input-group-addon span-width">Tratamiento Actual.</span>
                      <input type="text" class="form-control" aria-label="...">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="input-group ">
                      <span class="input-group-addon">
                          <input type="checkbox" aria-label="">
                      </span>
                      <span class="input-group-addon span-width">Tratamiento Actual.</span>
                    </div>
                  </div>
                </div>
                <div class="row form-group">
                  <table class="table">
                     <thead>
                       <tr>
                         <th>Descripccion</th>
                         <th>Año</th>
                         <th>Tratamiento Actual</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <td></td><td></td><td></td>
                       </tr>
                     </tbody>
                  </table>
                </div>


              </div>
          </div>
      </div>




        <div class="box box-default">
            <div class="box-header with-border">Información de Paciente
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row col-md-12">

                  <div class="row form-group">
                    <hr>
                    <h5> <strong> Accidentes Particulares </strong>  </h5>
                    <div class="col-md-8">
                      <div class="input-group">
                        <span class="span-width input-group-addon" style="padding-top:8px;padding-bottom:9px;">Descripción &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp</span>
                        <input type="text" class="form-control" aria-label="...">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="input-group ">
                        <span class="input-group-addon span-width">Año Dx.</span>
                        <input type="text" class="form-control" aria-label="...">
                      </div>
                    </div>
                    <div class="col-md-1">
                      <button type="button" class="btn btn-success"><span><i class="fa fa-plus" aria-hidden="true"></i></span></button>
                    </div>
                  </div>
                  <div class="row form-group">
                    <div class="col-md-8">
                      <div class="input-group ">
                        <span class="input-group-addon span-width">Tratamiento Actual.</span>
                        <input type="text" class="form-control" aria-label="...">
                      </div>
                    </div>
                  </div>
                  <div class="row form-group">
                    <table class="table">
                       <thead>
                         <tr>
                           <th>Descripccion</th>
                           <th>Año</th>
                           <th>Tratamiento Actual</th>
                         </tr>
                       </thead>
                       <tbody>
                         <tr>
                           <td>Accidente particular</td><td>2005</td><td>tratamiento de accidente particular</td>
                         </tr>
                       </tbody>
                    </table>
                  </div>


                </div>
            </div>
        </div>





</div>
