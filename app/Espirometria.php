<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Espirometria extends Model
{
    protected  $table = "espirometrias";
    public $timestamps = false;
}
