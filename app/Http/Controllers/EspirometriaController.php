<?php

namespace App\Http\Controllers;

use App\Espirometria;
use Illuminate\Http\Request;

class EspirometriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Espirometria  $espirometria
     * @return \Illuminate\Http\Response
     */
    public function show(Espirometria $espirometria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Espirometria  $espirometria
     * @return \Illuminate\Http\Response
     */
    public function edit(Espirometria $espirometria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Espirometria  $espirometria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Espirometria $espirometria)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Espirometria  $espirometria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Espirometria $espirometria)
    {
        //
    }
}
