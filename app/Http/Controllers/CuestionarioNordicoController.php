<?php

namespace App\Http\Controllers;

use App\CuestionarioNordico;
use Illuminate\Http\Request;

class CuestionarioNordicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CuestionarioNordico  $cuestionarioNordico
     * @return \Illuminate\Http\Response
     */
    public function show(CuestionarioNordico $cuestionarioNordico)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CuestionarioNordico  $cuestionarioNordico
     * @return \Illuminate\Http\Response
     */
    public function edit(CuestionarioNordico $cuestionarioNordico)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CuestionarioNordico  $cuestionarioNordico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CuestionarioNordico $cuestionarioNordico)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CuestionarioNordico  $cuestionarioNordico
     * @return \Illuminate\Http\Response
     */
    public function destroy(CuestionarioNordico $cuestionarioNordico)
    {
        //
    }
}
