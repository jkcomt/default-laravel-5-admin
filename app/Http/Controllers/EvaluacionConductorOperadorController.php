<?php

namespace App\Http\Controllers;

use App\EvaluacionConductorOperador;
use Illuminate\Http\Request;

class EvaluacionConductorOperadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EvaluacionConductorOperador  $evaluacionConductorOperador
     * @return \Illuminate\Http\Response
     */
    public function show(EvaluacionConductorOperador $evaluacionConductorOperador)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EvaluacionConductorOperador  $evaluacionConductorOperador
     * @return \Illuminate\Http\Response
     */
    public function edit(EvaluacionConductorOperador $evaluacionConductorOperador)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EvaluacionConductorOperador  $evaluacionConductorOperador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EvaluacionConductorOperador $evaluacionConductorOperador)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EvaluacionConductorOperador  $evaluacionConductorOperador
     * @return \Illuminate\Http\Response
     */
    public function destroy(EvaluacionConductorOperador $evaluacionConductorOperador)
    {
        //
    }
}
