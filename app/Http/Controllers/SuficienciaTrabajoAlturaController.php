<?php

namespace App\Http\Controllers;

use App\SuficienciaTrabajoAltura;
use Illuminate\Http\Request;

class SuficienciaTrabajoAlturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SuficienciaTrabajoAltura  $suficienciaTrabajoAltura
     * @return \Illuminate\Http\Response
     */
    public function show(SuficienciaTrabajoAltura $suficienciaTrabajoAltura)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SuficienciaTrabajoAltura  $suficienciaTrabajoAltura
     * @return \Illuminate\Http\Response
     */
    public function edit(SuficienciaTrabajoAltura $suficienciaTrabajoAltura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SuficienciaTrabajoAltura  $suficienciaTrabajoAltura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SuficienciaTrabajoAltura $suficienciaTrabajoAltura)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SuficienciaTrabajoAltura  $suficienciaTrabajoAltura
     * @return \Illuminate\Http\Response
     */
    public function destroy(SuficienciaTrabajoAltura $suficienciaTrabajoAltura)
    {
        //
    }
}
